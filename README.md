# FormProject
Lazarus Pascal form
Generic template for forms projects in Lazarus Pascal on Linux/ReactOS/GhostBSD using native controls

![FormProject.png](./FormProject.png?raw=true "Screenshot")


ReactOS compatibility:
ReactOS v0.4.13 using Lazarus IDE 2.0.12 and FPC 3.2.0 (i386-win32).

GhostBSD compatibility:
GhostBSD 2024.04.1 using Lazarus IDE 3.2 and FPC 3.2.2 (x86_64-freebsd-gtk2).
